#coding=utf-8
# SIMPLE HMI CON BOKEH
#Pelayo Leguina
######################## INSTRUCCIONES PARA LA EJECUCIÓN DE ESTE SCRIPT ###################
# 1. Abrir la terminal anaconda prompt
# 2. Abrir el directorio dónde se encuentre este script haciendo uso del comando: cd (copia de la ruta del script)
# 3. Ejecutar: bokeh serve simpleHMI.py
# 4. Abrir un navegador e introducir la dirección http://localhost:5006
import sqlite3
import os
import datetime


################################## CONFIGURACIÓN OPC UA ################################

from opcua import Client
from opcua import ua # Si no se encuentra el módulo opcua ejecutar en anaconda prompt: pip install opcua

# SUSTITUIR LA IP POR LA DE VUESTRO AUTÓMATA
client = Client("opc.tcp://10.0.0.42:4840")              # construye un objeto cliente OPC UA para un servidor
# Función para conectar el cliente al servidor OPC UA de nuestro autómata
def connect():
    client.connect()
    # navegación a través del árbol de objetos 
    root = client.get_root_node()                               
    objects = root.get_child(['0:Objects'])
    m241=objects.get_child(['2:M241-M251 data'])
    return m241

#################################### HMI CON BOKEH ############################
from bokeh.io import curdoc
from bokeh.layouts import  widgetbox,gridplot,row,column
from bokeh.models import ColumnDataSource,AnnularWedge
from bokeh.models.widgets import TextInput, Button, Slider
from bokeh.models import RadioButtonGroup

from bokeh.models import  Toggle
from bokeh.plotting import figure
from bokeh.models import PreText
from bokeh.models import  Dropdown

#para gauge
from math import cos, pi, sin

from bokeh.models import Arc, Circle,Range1d, Ray, Text,Plot

from bokeh.models import Div


i = 0  # Inicio de un contador
x = [] # Columna x de los datos. Esta columna se rellenara con el contador i
y = [] # Columna y de los datos. Esta columna se rellenara con el valor de la variable del tipo int definida en el autómata
 # SUSTITUIR LA DIRECCIÓN DE LA VARIABLE OPC POR LA VUESTRA (MIRAR EL NOMBRE EN EL UA EXPERT)
Start="GVL.INICIO_MEZCLA"
VariableOn = "Programa_principal.ON"
VelocidadPrograma = "GVL.VELOCIDAD_MEZCLA"
TiempoPrograma = "GVL.TIEMPO_MEZCLA"
Manual = "GVL.MAN"
Parada ="GVL.PARADA"
Sentido="GVL.SENTIDO_GIRO"
Turbo="GVL.TURBO_ON"
Averia="GVL.AVERIA_ON"
Alterno="GVL.MODO_ALTERNO_ACTIVADO"
Velocidad_pantalla="GVL.VELOCIDAD_POR_PANTALLA"
Tiempo_pantalla="GVL.TIEMPO_POR_PANTALLA"
Tiempo_pantalla_D="GVL.TIEMPO_ALTERNO_IZQUIERDA_PANTALLA"
Tiempo_pantalla_I="GVL.TIEMPO_ALTERNO_DERECHA_PANTALLA"
Velocidad_manual="GVL.VEL_POT_PANTALLA"
Reset="GVL.RESET_LEX"
Tiempo_inverted="GVL.Y"

# Objeto para el manejo del documento html que sirve bokeh
doc = curdoc()
source = ColumnDataSource(data=dict(x=x, y=y))
# Figura para el estado de una variable del tipo INT
# La información de la variable está almacenada en source
plot_vel = figure(plot_height=400, plot_width=600, title="Valor consigna vel",
              tools="pan,wheel_zoom")
# Línea con la que se representan los datos. En una misma figura se puede integrar más de una figura.
plot_vel.line('x', 'y', source=source, line_width=3, line_alpha=0.6)
plot_vel.title.text="Velocidad mezcla"
plot_vel.title.align = "center"
plot_vel.title.text_color = "pink"
plot_vel.title.text_font_size = "30px"




#VARS
Empresa=("Default")
Nif=("Default")
Telefono=("Default")
Producto=("Default")
Producto2=("Default")
Tiempo=0
Velocidad=0
Man=False
ManAuto=0
Tiempo_alD=0
Tiempo_alI=0
PARO=False
counter=0

lista_verificacion=[False,False,False,False,False,False,False]
lista_verificacion_alterno=[False,False]

##########################FOTO##########################
x_range = (-20,-10) # could be anything - e.g.(0,1)
y_range = (20,30)
pic = figure(x_range=x_range, y_range=y_range)
#img_path = 'https://docs.bokeh.org/en/latest/_static/images/logo.png'
img_path = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAWwAAACKCAMAAAC5K4CgAAAA7VBMVEX///8AAAD+AAD//v/29vbn5+f8/Pzq6ur4+Pjz8/Nqamq+vr7a2tpDQ0NxcXF4eHiIiIjQ0NCdnZ2jo6O2trYaGho5OTmVlZX/+fn/7u7/5+f/9PR/f3+wsLCqqqrNzc0wMDBdXV3/4OD+Skr+LCyQkJBZWVlKSkohISH/1dX+OTn/v78TExMeHh7/zMz/pqb+ICH/hof+tbb+ZGT/n5/+VVX+Pz//j47+fn7/ZGR+AAH+t7f+Fxf+2tv+c3L+SUn+lJQuAADqAAC5AAFTAAE9AAFWREPcAACwAQFXAAGJAQEgAAHLAAKYAAH/eHikQBk0AAAL0UlEQVR4nO1diXbiOBY1YknYycaesJmwFhCISTIhhK7unp6e7sn/f85IlizJkghMVVKomHfPqXNiSdjy1fPTfU+yy3EAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJsQPXQHjh9lAuD58xFtT5eIwPUWd8/VQ3fnmNG+H6IQOvNK4tCdOjpEiXuerJEBj4fu2zGi2jFRjbGoHbprx4aXxRaqMVaDQ/fueEB0x3Q71QSTQ/fxiDBYvs81Qm2Q3B+EyS6qEZoduo/HgvvdXCO0OXQvjwI1bx+uEaocuqPHgJ3umgHk9vdjT7sG0/4AvKOuVcwP3defHU/7c40WoP6+C/8L1whBRup7sM2HDJfLoaG4Bqb97XgwU71uE07bd1pF+9Ad/okxMnM9CuoHag0kSL4ZZddEdUdKp86VurvDdfZnh2a4BA+hJmotLJJ9K3SfrDkKtcmD+UyAnXjVuQ6FLVE9ye0eqq8/PWYa1+twg6hTVRqsIGL/Rqw0sjUdrZINTvtboS3vLrQmKtmz8gH6eRTQlgz0VV2VbH04APtBZdKQr1aXy0Z6E8B+2ISIXKppJuy/38Blfxhkrz3UN+JENcFygD4eDcpilebOlM8rK1x7P7yHR4WXjee6rjc16eeo86yQ/fzD+/d/BHV5EoTfB2DLkkBb4Xptbgb4CKhrZiD8Pg+qyF7CmtinIaquQlpp2JluLpU/PzFXnqXTmZN4TC66rKdS9XQmHj8xrF5nuq1sr5vZfdH6bTZVOpNKkhf5VO5iSzec+PUF6WZj+xnV9OrKxh3x9QjFjaGuUWSVKc5rYxyRUEx1b9ISQfmgolVIb99IcHaTZc168aDskpVcGn9xHZw3v+2cNYVrK9fE6pw43RzTgtUeK8pETAjoToVKx83sl36/37wtXCalsyZv5UZn6qXShk5eiPZxQzWBtivNwlS2RF5WrUvKpLAHuGUku0jJbBgrCa7ESCaKoZo6Le2JpoZeSu2vzfexUbm2MeF3I91HQamT7CkSOadlZi6zhh+E0eNnDVt/pOQXyuOqP2FxqfbWeBv6Lgcb94ycyzeuWE0zot/kFi7pw/0O2ePA06SVCupGklqJjEv5B6eGu9DfRLAyoAmRHQlN9iEvEmnSm7wyUslmtUtjJUVgr2EnEmnp19LJLhiuJaOtcW2lYav8yMqrG66ibjlnIjKQCEnzUMhnVq7XZMWnfVGmT4GhAdL9SEXfvWPnRvgz5ebFQ3qisEU5kHx8r1vqXp93MbiXNWsVghxr0QuVprhKkco1sk9CajOSVKoNXNsoRRzVV/DH2pGkLQOVZNLgqDftn67+BfvnK83Cs2wQ5RHMyEFRS5RrTlkROUpgY+J6+t28fA4UHypkbk+poEGPxNYWxZs8wfFmMpM5kz1tQKBQ9dlwrCi5J+2M4XlFEU2mt/SG38HHp6IVvpNInxms5hE0jbYtsg4giZnAGGO8pBgLNxZk6zpb6WIoHlD3UrLZ0dIclEp2YDh1tZxGHzHhP3ckQCQ9XQrKxPSoSg5BthZbyZOnD+mRMr5UTTJQdrKdj6jwSVR9eUC2FP+9T7YkO8Q8wI09pTYXZGtVmqAUAb1pr6W1DttRNKwwra5WTCVXIssLerk8QclIuqQgrvhMKlyT5oLeIVtTm4HTrhjf+iAJKDvtWpt9CEjYoJcWaftbvSZmOK3khcRglIKinNZejLlGtupFgp6M9M1/yFaFzWAg++pUC6ojfOIykF3Szyo9+lIOgAscXcgI/a6SzRMjRf5z37TN38B4+yhePgWmfEZBE4QEVBXrIWTzXMtcS5kjaSRigctuqe3llElRqeHWcC4GJLPtGxhW2/WW5JHusd8hO6VHN0Kjyw6Da/QLvRsicOkrNXySyIhJO7/lvTHLuTa5kRC+8L/os6+TrS+eiAEsykbP50fD+oAk68MVp7z8Is3FzC9mrj3bN4oIskMZ1QA9Qe42srU0nBReh4yeW7ZhJVFyPOEKJXnD8NXMNvm43/puMx2N7NxSKaaygsnIG+qymSB7XGzlLy7PtFSGJNHDqpDLQYNY3Ep2KWLCr9vIFrDxm1xiZsoblMat5BJo0McDw/G2cF0occWCuWUb3MhWsvWgi+Afu8l+rdqntwXZ3dDyU0CXuhLLB6S35YSCHXW9Phn4f4MaEZdW1IgYOhm/7Sbbxo0jwnJv9Mi4Lw+G75tFBGlgjEDoM01ziFD/vWXG4pbyEH7fh2z7NjMIn0gsV1mNvZFlgkK2ujxMEeOEGpbB+VOhyjuZ1PAgSpPtuJn952/BwR87qZ5ZuIQgJkDik0UOlICs2wgWfDdyyqWgQSw7UhLRtNlGzMBsA4PDlaG4TDiU5w9KI35y6tz/Ghz+ayfZHfIBNMuctrBl/9kOORJiZEmuCH2yRYq1m4gxCD1yxh12L41xfV7AKF03mAKUfMItEZKXvX4zl1SqwuF6IH7oDIHQn+z43/v4Eevyf0KB0HwSN01mncLRKmRLaF74fCdShjo2MvRisrToXdNL9339Lq8zFlu57iUdQX51P+pvI/QfdvznPmRbt8TOI+srentxcdfUs3In7Us/I9lMeKgLaTKYBzdV+Q4nqeb2vvgWzw3+jOi4AUKBH/lrD67t2xTFGQoi6xa/XzoFcnP1VXPYqXMQqzdkCgWYkza18QdRI5tePXDZdNUfk/1HUL1rhlxa6EU0yxZasEmPeabZdyPGzQrNC2LZ5vCDIVjs0tcqaBCT0PKMY1LcYgc0/4LdyNe/WMnfO7ieD+3zIoLsMSM7yW57zMQw14b+zGU0377vCUxpWQ6+tUZb22SqRo9eyZMW/M1iTsziL6zk6/tku2hq4f5szmUzIRXlcr0gGmdP8piSbbRs2vY9riXVraRve4nQZZThCQ6YnHnmM+SueH1m5UuQp8EEKAXXGTmpQW356rxArUtLDI2zvYbUUAETjnIaNi7R3Sxw3ajG5f7Twnb7cO19z8RfmOvl+mE6mjxPR/NJezDtuHeDTaVt5Uf9kr4bbZr2oFP4LIhDpsT7t7l66Tp9Jm1rEuMwvs3nC+lGPHaaSDa6uXMlPD8p0TEoyhdNhB0MHXuaYB2LTO1giP74m/nrYWfoPU5HbdVfWEkzRzJm2krGa1vNfiidEY/HTft2MdKFev06vmv3Dr1kPK5dNBnDpfE4jpN41ellqRTeYzK58x42E+si8Q9EYx8CAYBjhJ/ziUaD3I+eAjIkhcxFe2SPLEswHRBzz0KJexRItMvEzqpkhbWGY7ZqwnmlSyQ1PImVa7TUqZADv7hd9YeiUq1R1VCrYpAzELFWbldpK/zHj78X6/FANmuQr654iUQHtaf40KUv1w7dmvM0qzhDNHLKyHWmK8LfBKHVokY++LRCL6SZv5f9voLPsHwhb9jNNrhwhEvX4CtUvJKPA62RtyAso8EcLZ0VXSIhn2haomrNf3VriAaeX9xG6BHNCNnPGz95UX1A61FliF49XDxH7hq9kYzTegYf91RBPknWrpDYeIkqmPc2ZmyGqGVjcp9QZYSQW3ZGaEn3K5VXqPqMNgkyMHS3wQg9Y5I9YuOVAf75G3rx0BwTbmPAfVA8Iw89vpCF7DvU9i125nQCsp/WQ1R7Q6+kesNy+ZjsQQXdk+8zdyjZE3SPndGcvBFdecHjNUID8l3PKpCt4g65aFlF0/lw5mKH4VQw2ctVQPZihrCDcMk69wQ9+T/AZL+M0KTszpzyxJdwE9+yH53oDBH/4by9VjtogIvt3pF6AAw72GYr3mpNtml0UAd7b+yzfbJd7D0WaIJd7+vQqbnsy6nYZ78hNCjP0PqJfjCLkI3H6fEJj8kcrd6QWxuh1cbOPU0HBZo6A69We3JdTGptPXOfqs7aSxBe117ZmbojrDnuH52XYMtMxXOX64lT6wyX7P/IHBBV0vZmHuZ+MnQ7d1i0TIez9csBb8tO8H2j0QWhU9tGmnC0AIe1UYVdlP4LUnZ2p+4OjdoCHvsfCYhCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgI/CfwHTX9Lg/g01vwAAAABJRU5ErkJggg=='
pic.image_url(url=[img_path],x=x_range[0],y=y_range[1],w=x_range[1]-x_range[0],h=y_range[1]-y_range[0])
pic.toolbar_location = None
pic.outline_line_color = None
pic.xaxis.visible = None
pic.yaxis.visible = None
pic.xgrid.grid_line_color = None

####################################################################################
##################---------------GAUGE VEL------------##############################

xdr = Range1d(start=-1.25, end=1.25)
ydr = Range1d(start=-1.25, end=1.25)
#GAUGE VEL
plot = Plot(x_range=xdr, y_range=ydr, plot_width=600, plot_height=600)
plot.toolbar_location = None
plot.outline_line_color = None
#GAUGE TIME
plot_t = Plot(x_range=xdr, y_range=ydr, plot_width=600, plot_height=600)
plot_t.toolbar_location = None
plot_t.outline_line_color = None


start_angle = pi + pi/4
end_angle = -pi/4

max_value = 1000

max_value_t= 20


major_step, minor_step = 100, 50

major_step_t,minor_step_t = 5,1

plot.add_glyph(Circle(x=0, y=0, radius=1.00, fill_color="white", line_color="black"))
plot.add_glyph(Circle(x=0, y=0, radius=0.05, fill_color="gray", line_color="black"))

#gauge_time
plot_t.add_glyph(Circle(x=0, y=0, radius=1.00, fill_color="white", line_color="black"))
plot_t.add_glyph(Circle(x=0, y=0, radius=0.05, fill_color="gray", line_color="black"))

plot.add_glyph(Text(x=0, y=+0.15, text=["RPM"], text_color="red", text_align="center", text_baseline="bottom", text_font_style="bold"))


#gauge time
plot_t.add_glyph(Text(x=0, y=+0.15, text=["Tiempo"], text_color="blue", text_align="center", text_baseline="bottom", text_font_style="bold"))


def data(value):
    """Shorthand to override default units with "data", for e.g. `Ray.length`. """
    return dict(value=value, units="data")

def speed_to_angle(value):
    value = min(max(value, 0), max_value)
    total_angle = start_angle - end_angle
    angle = total_angle*float(value)/max_value
    return start_angle - angle
def speed_to_angle_t(value):
    value = min(max(value, 0), max_value_t)
    total_angle = start_angle - end_angle
    angle = total_angle*float(value)/max_value_t
    return start_angle - angle

def add_needle(value):
    angle = speed_to_angle(value)
    plot.add_glyph(Ray(x=0, y=0, length=data(0.75), angle=angle,    line_color="black", line_width=3))
    plot.add_glyph(Ray(x=0, y=0, length=data(0.10), angle=angle-pi, line_color="black", line_width=3))

def polar_to_cartesian(r, alpha):
    return r*cos(alpha), r*sin(alpha)

def add_gauge(radius, max_value, length, color, major_step, minor_step):
    

    major_angles, minor_angles = [], []

    total_angle = start_angle - end_angle

    major_angle_step = float(major_step)/max_value*total_angle
    minor_angle_step = float(minor_step)/max_value*total_angle

    major_angle = 0

    while major_angle <= total_angle:
        major_angles.append(start_angle - major_angle)
        major_angle += major_angle_step

    minor_angle = 0

    while minor_angle <= total_angle:
        minor_angles.append(start_angle - minor_angle)
        minor_angle += minor_angle_step

    major_labels = [ major_step*i for i, _ in enumerate(major_angles) ]

    n = major_step/minor_step
    minor_angles = [ x for i, x in enumerate(minor_angles) if i % n != 0 ]

    
    
    #Arc(x=0, y=0, radius=radius, start_angle=start_angle, end_angle=end_angle, direction="clock", line_color=color, line_width=2)
    #plot.add_glyph(glyph)

    rotation = 0 

    # Ticks de la escala mayores
    x, y = zip(*[ polar_to_cartesian(radius, angle) for angle in major_angles ])
    angles = [ angle + rotation for angle in major_angles ]
    source = ColumnDataSource(dict(x=x, y=y, angle=angles))

    glyph = Ray(x="x", y="y", length=data(length), angle="angle", line_color='black', line_width=2)
    plot.add_glyph(source, glyph)

    # Ticks de la escala menores
    x, y = zip(*[ polar_to_cartesian(radius, angle) for angle in minor_angles])
    angles = [ angle + rotation for angle in minor_angles ]
    source = ColumnDataSource(dict(x=x, y=y, angle=angles))

    glyph = Ray(x="x", y="y", length=data(length/2), angle="angle", line_color='black', line_width=1)
    plot.add_glyph(source, glyph)

    x, y = zip(*[ polar_to_cartesian(radius+2*length, angle) for angle in major_angles ])
    text_angles = [ angle - pi/2 for angle in major_angles ]
    source = ColumnDataSource(dict(x=x, y=y, angle=text_angles, text=major_labels))

    glyph = Text(x="x", y="y", angle="angle", text="text", text_align="center", text_baseline="middle")
    #plot.add_glyph(source, glyph)


#####ADD GAUGE_T
def add_gauge_t(radius, max_value, length, color, major_step, minor_step):
    

    major_angles, minor_angles = [], []

    total_angle = start_angle - end_angle

    major_angle_step = float(major_step)/max_value*total_angle
    minor_angle_step = float(minor_step)/max_value*total_angle

    major_angle = 0

    while major_angle <= total_angle:
        major_angles.append(start_angle - major_angle)
        major_angle += major_angle_step

    minor_angle = 0

    while minor_angle <= total_angle:
        minor_angles.append(start_angle - minor_angle)
        minor_angle += minor_angle_step

    major_labels = [ major_step*i for i, _ in enumerate(major_angles) ]

    n = major_step/minor_step
    minor_angles = [ x for i, x in enumerate(minor_angles) if i % n != 0 ]

    glyph = Arc(x=0, y=0, radius=radius, start_angle=start_angle, end_angle=end_angle, direction="clock", line_color=color, line_width=2)

    #gauge time
    plot_t.add_glyph(glyph)
    

    rotation = 0 

    # Ticks de la escala mayores
    x, y = zip(*[ polar_to_cartesian(radius, angle) for angle in major_angles ])
    angles = [ angle + rotation for angle in major_angles ]
    source = ColumnDataSource(dict(x=x, y=y, angle=angles))

    glyph = Ray(x="x", y="y", length=data(length), angle="angle", line_color=color, line_width=2)
 
    #gauge time
    plot_t.add_glyph(source, glyph)

    # Ticks de la escala menores
    x, y = zip(*[ polar_to_cartesian(radius, angle) for angle in minor_angles])
    angles = [ angle + rotation for angle in minor_angles ]
    source = ColumnDataSource(dict(x=x, y=y, angle=angles))

    glyph = Ray(x="x", y="y", length=data(length/2), angle="angle", line_color=color, line_width=1)

    #gauge t
    plot_t.add_glyph(source, glyph)

    x, y = zip(*[ polar_to_cartesian(radius+2*length, angle) for angle in major_angles ])
    text_angles = [ angle - pi/2 for angle in major_angles ]
    source = ColumnDataSource(dict(x=x, y=y, angle=text_angles, text=major_labels))

    glyph = Text(x="x", y="y", angle="angle", text="text", text_align="center", text_baseline="middle")
  
    #gauge t
    plot_t.add_glyph(source, glyph)    

add_gauge(0.75, max_value, 0.03,  "red", major_step, minor_step)

add_gauge_t(0.75,max_value_t,0.05,"blue",major_step_t,minor_step_t)

# ARCOS
#angle_needle = [speed_to_angle(30)]
angle_arc = [speed_to_angle(0)]
speed_arc =[0]
colour=['green']
#source_needle = ColumnDataSource(dict( angle=angle_needle))
source_arc =ColumnDataSource(dict(angle=angle_arc))
source_arc_amarillo=ColumnDataSource(dict(angle=angle_arc))
source_arc_rojo=ColumnDataSource(dict(angle=angle_arc))
source_text=ColumnDataSource(dict(text=speed_arc,color=colour))

a1=plot.add_glyph(source_arc,AnnularWedge(x = 0,y = 0, inner_radius = 0.85, outer_radius = 0.6,start_angle = start_angle, end_angle = 'angle',fill_color = "green",direction='clock'))
a2=plot.add_glyph(source_arc_amarillo,AnnularWedge(x = 0,y = 0, inner_radius = 0.85, outer_radius = 0.6,start_angle = pi/2, end_angle = 'angle',fill_color = "yellow",direction='clock'))
a3=plot.add_glyph(source_arc_rojo,AnnularWedge(x = 0,y = 0, inner_radius = 0.85, outer_radius = 0.6,start_angle = 0, end_angle = 'angle',fill_color = "red",direction='clock'))
a4=plot.add_glyph(source_text,Text(x=0.01,y=-0.8,text='text',text_align='center',text_font_style='bold',text_font_size='60pt',text_color='color'))

a1.visible=False
a2.visible=False
a3.visible=False

##gauge T
angle_needle_t = [speed_to_angle_t(20)]
source_needle_t = ColumnDataSource(dict( angle=angle_needle_t))
plot_t.add_glyph(source_needle_t,Ray(x=0, y=0, length=data(0.75), angle="angle", line_color="black", line_width=3))


# WIDGETS
# Son los elementos interactivos que usaremos para visualizar las variables del autómata y actuar sobre ellas 
valor_vel=0

div = Div(text=""" <b><big><big>**************************************************</big></big></b></br><u><b><big><big>TRABAJO INTEGRACION DE SISTEMAS</big></big></b></br></u><b><big><big>**************************************************</big></big></b></br>
          <p><em><big>Instrucciones: </big></em></p> * Los controles de la pantalla solo funcionan para el modo HMI.</br>                 * El slider marca la velocidad del motor de la operacion actual</br>     
          * Hay que asegurarse de rellenar los campos CORRECTAMENTE</br> <p><em><big>Funciones: </big></em></p>  *<b>TURBO</b>: La consigna pasa a ser de 1000 si ON</br>
           *<b>AVERIA</b>: La velocidad actual pasara a 0 lentamente si ON</br>  *<b>ALTERNO</b>: El motor alternara el giro de derecha a izquierda durante los tiempos que se determinen.</br>
          
""",
width=400, height=400)


#DATOS BDD
pre_text_bdd= PreText(text="RELLENE SU INFORMACION",style={'color':'blue'})
pre_text_consola_intro=PreText(text="INFORMACION DEL SISTEMA :")
pre_text_consola_info=PreText(text="BIENVENIDO",style={'color':'green'})
pre_text_fila1=PreText(text="***********************************")
pre_text_fila2=PreText(text="***********************************")
pre_text_alterno=PreText(text="TIEMPO DERECHA")
pre_text_alterno2=PreText(text="TIEMPO IZQUIERDA")
text_empresa=TextInput(title="Nombre de la Empresa", value='Introduzca nombre')
text_NIF=TextInput(title="NIF", value='Introduzca NIF') 
text_tel=TextInput(title="Telefono", value='Introduzca telefono') 


text_producto=TextInput(title="Producto 1 a mezclar", value='Introduzca el nombre del producto')  
text_producto2=TextInput(title="Producto 2 a mezclar", value='Introduzca el nombre del producto') 
b_limpiar=Button(label="LIMPIAR DATOS") 

menu = [("Item 1", "item_1"), ("Item 2", "item_2"), None, ("Item 3", "item_3")]
dropdown = Dropdown(label="Dropdown button", button_type="warning", menu=menu)
#CONTROL_MOTOR
ETIQUETAS=["HMI","MANUAL"]
ETIQUETAS2=["DERECHA","IZQUIERDA"]
b_am= RadioButtonGroup(labels=ETIQUETAS,active=0)
b_giro= RadioButtonGroup(labels=ETIQUETAS2,active=0)


text_tiempo=TextInput(title="Tiempo de Mezcla", value='0') 
text_velocidad = TextInput(title="Velocidad de mezcla", value='0')       # Textbox para introducir un nuevo valor a nuestra variable del tipo INT

slider_vel= Slider(title="Velocidad actual",value=valor_vel,start=0,end=750,step=10)

b_turbo= Toggle(label="TURBO",button_type="warning")
b_averia= Toggle(label="AVERIA",button_type="primary")
b_start= Button(label="START",button_type="success")
b_stop= Button(label="STOP",button_type="danger")
b_alterno=Toggle(label="MODO ALTERNO",button_type="default")
b_start_alterno=Toggle(label="START ALTERNO",button_type="success")
text_tiempo_der=TextInput(title="Tiempo giro derecha", value='0') 
text_tiempo_izq=TextInput(title="Tiempo giro izquierda", value='0') 

b_reset = Button(label="reset")        # Botón para el flag de set. Mediante el parámetro button_type se indica el color del botón 
b_power = Button(label = "Power",button_type="danger")  # Botón para el flag de reset

imputs_bdd=widgetbox(pre_text_bdd,text_empresa,text_NIF,text_tel,text_producto,text_producto2,text_tiempo,text_velocidad,b_limpiar)
imputs_control=widgetbox(b_reset,b_power,b_am,b_giro,b_turbo,b_averia,b_start,b_stop,b_alterno,pre_text_alterno,text_tiempo_der,pre_text_alterno2,text_tiempo_izq)
imputs_info=widgetbox(pre_text_fila1,pre_text_consola_intro,pre_text_fila2,pre_text_consola_info)





# ----------------------- CALLBACKS ----------------------------------------
def update_limpiardatos():
    global Empresa,Nif,Telefono,Producto,Producto2,Tiempo,Velocidad,text_empresa,text_NIF,text_producto,text_producto2,text_tel,text_tiempo,text_velocidad,lista_verificacion,lista_verificacion_alterno,text_tiempo_der,text_tiempo_izq
    
    Empresa=("Default")
    Nif=("Default")
    Telefono=("Default")
    Producto=("Default")
    Producto2=("Default")
    Tiempo=0
    Velocidad=0
    text_empresa.value=''
    text_NIF.value=''
    text_tel.value=''
    text_producto.value=''  
    text_producto2.value=''
    text_tiempo.value='0'
    text_velocidad.value='0'
    text_tiempo_der='0'
    text_tiempo_izq='0'
    pre_text_consola_info.style={'color':'green'}
    pre_text_consola_info.text='INTRODUZCA DATOS'
   
    
    lista_verificacion=[False,False,False,False,False,False,False]
    lista_verificacion_alterno=[False,False]
b_limpiar.on_click(update_limpiardatos)
def update_empresa(attrname,old,new):
    global Empresa,pre_text_consola_info,lista_verificacion
    if text_empresa.value == Empresa:
        pre_text_consola_info.style={'color':'red'}
        pre_text_consola_info.text='LIMPIE DATOS'
        lista_verificacion[0]=False
        print("error en cambio")
    else:
        lista_verificacion[0]=True
        pre_text_consola_info.style={'color':'green'}
        pre_text_consola_info.text='EMPRESA INTRODUCIDA CORRECTAMENTE'
        print("Cambio nombre empresa")
        Empresa=(text_empresa.value)
        print(Empresa)
text_empresa.on_change('value', update_empresa)

def update_NIF(attrname,old,new):
    global Nif,pre_text_consola_info,lista_verificacion
    letras_posibles='A','B','C','D','E','F','G','H','J','P','Q','R','S','U','V'
    letras=0
    for letra in letras_posibles:
        if letra in text_NIF.value:
            letras=letras+1
    if letras != 1:
        pre_text_consola_info.style={'color':'red'}
        pre_text_consola_info.text='FORMATO NIF ERRONEO'
        lista_verificacion[1]=False
        print("error en cambio")
    else:
        pre_text_consola_info.style={'color':'green'}
        pre_text_consola_info.text='NIF INTRODUCIDO CORRECTAMENTE'
        lista_verificacion[1]=True
        print("Cambio NIF")
        Nif=(text_NIF.value)
        print(Nif)

text_NIF.on_change('value', update_NIF) 
def update_telef(attrname,old,new):
    global Telefono,pre_text_consola_info
    if text_tel.value.isdigit() == False:
         pre_text_consola_info.style={'color':'red'}
         pre_text_consola_info.text='TELEFONO ERRONEO'
         print("error en cambio")
         lista_verificacion[2]=False
    else:
        lista_verificacion[2]=True
        
        
            
        pre_text_consola_info.style={'color':'green'}
        pre_text_consola_info.text='TELEFONO INTRODUCIDO CORRECTAMENTE '
        print("Cambio Telefono")
        Telefono=(text_tel.value)
        print(Telefono)
        if lista_verificacion[0]==True and lista_verificacion[1]==True and lista_verificacion[2]==True:
            conn=sqlite3.connect('BDD_MEZCLADORA.db')
            c=conn.cursor()
            c.execute("SELECT NOMBRE FROM EMPRESAS where Nombre = ? and NIF = ?",(Empresa,Nif))
            res=c.fetchall()
            if res==[]:
        
                c.execute("INSERT INTO EMPRESAS (Nombre,NIF,Telefono) VALUES (?,?,?)",(Empresa,Nif,Telefono))
                conn.commit()
                conn.close()
text_tel.on_change('value', update_telef)  

def update_producto1(attrname,old,new):
    global Producto,pre_text_consola_info,lista_verificacion
    if any(chr.isdigit() for chr in text_producto.value):
        pre_text_consola_info.style={'color':'red'}
        pre_text_consola_info.text='PRODUCTO NO PUEDE CONTENER NUMEROS'
        print("error en cambio")
        lista_verificacion[3]=False
    else:
        lista_verificacion[3]=True
        pre_text_consola_info.style={'color':'green'}
        pre_text_consola_info.text='PRODUCTO 1 INTRODUCIDO CORRECTAMENTE'
        print("Cambio en Producto 1")
            
        Producto=(text_producto.value)
        print(Producto)
  
        
text_producto.on_change('value', update_producto1)

def update_producto2(attrname,old,new):
    global Producto2,pre_text_consola_info,lista_verificacion
    if any(chr.isdigit() for chr in text_producto2.value) or text_producto2.value==Producto:
        pre_text_consola_info.style={'color':'red'}
        pre_text_consola_info.text='PRODUCTO NO PUEDE CONTENER NUMEROS'
        print("error en cambio")
        lista_verificacion[4]=False
    else:
        lista_verificacion[4]=True
        pre_text_consola_info.style={'color':'green'}
        pre_text_consola_info.text='PRODUCTO 2 INTRODUCIDO CORRECTAMENTE'
        print("Cambio en Producto 2")
        Producto2=(text_producto2.value)
        print(Producto2)
        if lista_verificacion[3]==True and lista_verificacion[4]==True:
            conn=sqlite3.connect('BDD_MEZCLADORA.db')
            c=conn.cursor()
            c.execute("SELECT * FROM PRODUCTOS where Producto_1 = ? and Producto_2 = ? ",(Producto,Producto2))
            conn.commit()
            res=c.fetchall()
            if res ==[]:
                c.execute("INSERT INTO PRODUCTOS (Producto_1,Producto_2) VALUES (?,?)",(Producto,Producto2))
                conn.commit()
text_producto2.on_change('value', update_producto2)
  
def update_velocidad(attrname, old, new):
#  Callback para el evento asociado al textbox
    print("Cambio valor velocidad")
    global Velocidad,pre_text_consola_info
    if text_velocidad.value.isdigit() == False or int(text_velocidad.value) <0 or int(text_velocidad.value)>750:
        pre_text_consola_info.style={'color':'red'}
        pre_text_consola_info.text='LA VELOCIDAD DEBE SER UN VALOR ENTERO ENTRE 0 Y 750'
        print("error en cambio")
        lista_verificacion[5]=False
    else:
        lista_verificacion[5]=True
        pre_text_consola_info.style={'color':'green'}
        pre_text_consola_info.text='VELOCIDAD INTRODUCIDA CORRECTAMENTE'
        
        Velocidad=int(text_velocidad.value)
        
        print(Velocidad)        
            

        newValue = int(Velocidad)
        m241 = connect()
        entero=m241.get_child(['2:'+ Velocidad_pantalla])      
        dv = ua.DataValue(ua.Variant(newValue,ua.VariantType.Int16))
        entero.set_value(dv)
        client.disconnect()
   
# Asociación de la callback update_entero al evento del textbox
text_velocidad.on_change('value', update_velocidad)

def update_tiempo(attrname, old, new):
#  Callback para el evento asociado al textbox
    print("Cambio tiempo velocidad")
    global Tiempo,pre_text_consola_info,lista_verificacion
    if text_tiempo.value.isdigit() == False or int(text_tiempo.value) < 0 or int(text_tiempo.value)>20:
        pre_text_consola_info.style={'color':'red'}
        pre_text_consola_info.text='EL TIEMPO MAXIMO SON 20 SEG'
        print("error en cambio")
        lista_verificacion[6]=False
    else:
        lista_verificacion[6]=True
        pre_text_consola_info.style={'color':'green'}
        pre_text_consola_info.text='TIEMPO INTRODUCIDO CORRECTAMENTE'
        Tiempo=int(text_tiempo.value)*1000
        print(Tiempo)

        newValue = int(Tiempo)
        m241 = connect()
        entero=m241.get_child(['2:'+ Tiempo_pantalla])      
        dv = ua.DataValue(ua.Variant(newValue,ua.VariantType.Int16))
        entero.set_value(dv)
        client.disconnect()
 
# Asociación de la callback update_entero al evento del textbox
text_tiempo.on_change('value', update_tiempo)

def update_START():
    print("Pulsado boton Start")
    global lista_verificacion,slider_vel
    i=0   
    for verificacion in lista_verificacion:
        if verificacion:
            i=i+1
    if i == len(lista_verificacion):
        pre_text_consola_info.style={'color':'green'}
        pre_text_consola_info.text='OPERACION GUARDADA. COMENZANDO....'
        
        m241 = connect()
   
        set_=m241.get_child(['2:' + Start])      
        state = set_.get_value()
        # Toggle en el valor de la variable set
        new_state = True if state == False else False
        # Escritura del nuevo estado
        dv = ua.DataValue(ua.Variant(new_state,ua.VariantType.Boolean))
        set_.set_value(dv)
    
        # Desconexión del cliente
        client.disconnect()
                   
        conn=sqlite3.connect('BDD_MEZCLADORA.db')
        c=conn.cursor()
        c.execute("SELECT * FROM EMPRESAS where Nombre = ? and NIF = ?",(Empresa,Nif))
        indice_empresas=c.fetchone()
        id_empresa=indice_empresas[0]
        print(id_empresa)
        c.execute("SELECT * FROM PRODUCTOS where Producto_1 = ? and Producto_2 = ?",(Producto,Producto2))
        indice_productos=c.fetchone()
        id_productos=indice_productos[0]
        print(id_productos)
        Fecha=datetime.datetime.now().strftime("%Y-%m-%d")
        print(Fecha)
        c.execute("SELECT * FROM OPERACIONES where Empresa_ID = ? and Productos_ID = ?",(id_empresa,id_productos))
        res=c.fetchall()
        if res==[]:
           c.execute("INSERT INTO OPERACIONES (Empresa_ID,Productos_ID,Velocidad,Tiempo,fecha) VALUES (?, ?, ?, ?, ?)",(id_empresa,id_productos,Velocidad,Tiempo,Fecha))
           conn.commit()
        conn.close()
           
    else:
        pre_text_consola_info.style={'color':'red'}
        pre_text_consola_info.text='ERROR, COMPRUEBE QUE LOS DATOS SON CORRECTOS E INTENTE DE NUEVO'
        print("error")
    
b_start.on_click(update_START)

def update_STOP():
    print("Stop pulsado")
    #STOP LEXIUM
    
    pre_text_consola_info.text='PARO PULSADO'
    
    m241 = connect()
   
    set_=m241.get_child(['2:' + Parada])      
    state = set_.get_value()
    # Toggle en el valor de la variable set
    new_state = True if state == False else False
    # Escritura del nuevo estado
    dv = ua.DataValue(ua.Variant(new_state,ua.VariantType.Boolean))
    set_.set_value(dv)
      
    # Desconexión del cliente
    client.disconnect()
    
    
b_stop.on_click(update_STOP)
def update_AVERIA(attr):
    if b_averia.active==1:
        
        print("Modo Averia")
        pre_text_consola_info.text='MODO AVERIA ACTIVADO'
    
        
         
        m241 = connect()
   
        set_=m241.get_child(['2:' + Averia])      
       
        # Escritura del nuevo estado
        dv = ua.DataValue(ua.Variant(True,ua.VariantType.Boolean))
        set_.set_value(dv)
    
        # Desconexión del cliente
        client.disconnect()
        
    if b_averia.active==0:
        print("Salir Modo Averia")
        pre_text_consola_info.text='REALIZE OPERACION'
    
    
        
        m241 = connect()
   
        set_=m241.get_child(['2:' + Averia])      
       
        # Escritura del nuevo estado
        dv = ua.DataValue(ua.Variant(False,ua.VariantType.Boolean))
        set_.set_value(dv)
    
        # Desconexión del cliente
        client.disconnect()
        
           
b_averia.on_click(update_AVERIA)


def update_tiempoAlternoD(attr,old,new):
    print("Cambio tiempo alterno derecha")
    global Tiempo_alD,pre_text_consola_info,lista_verificacion_alterno
    if text_tiempo_der.value.isdigit() == False or int(text_tiempo_der.value) < 0 :
        pre_text_consola_info.style={'color':'red'}
        pre_text_consola_info.text='EL TIEMPO DEBE SER UN VALOR ENTERO POSITIVO'
        print("error en cambio")
        lista_verificacion_alterno[0]=False
    else:
        lista_verificacion_alterno[0]=True
        pre_text_consola_info.style={'color':'green'}
        pre_text_consola_info.text='TIEMPO INTRODUCIDO CORRECTAMENTE'
        Tiempo_alD=int(text_tiempo_der.value)
        print(Tiempo_alD)
            
        newValue = int(Tiempo_alD*1000)
        m241 = connect()
        entero=m241.get_child(['2:'+ Tiempo_pantalla_D])      
        dv = ua.DataValue(ua.Variant(newValue,ua.VariantType.Int16))
        entero.set_value(dv)
        client.disconnect()
        
text_tiempo_der.on_change('value', update_tiempoAlternoD)
def update_tiempoAlternoI(attr,old,new):
    print("Cambio tiempo alterno derecha")
    global Tiempo_alI,pre_text_consola_info,lista_verificacion_alterno
    if text_tiempo_izq.value.isdigit() == False or int(text_tiempo_izq.value) < 0 :
        pre_text_consola_info.style={'color':'red'}
        pre_text_consola_info.text='EL TIEMPO DEBE SER UN VALOR ENTERO POSITIVO'
        print("error en cambio")
        lista_verificacion_alterno[1]=False
    else:
        lista_verificacion_alterno[1]=True
        pre_text_consola_info.style={'color':'green'}
        pre_text_consola_info.text='TIEMPO INTRODUCIDO CORRECTAMENTE'
        Tiempo_alI=int(text_tiempo_izq.value)
        print(Tiempo_alI)
        
        newValue = int(Tiempo_alI*1000)
        m241 = connect()
        entero=m241.get_child(['2:'+ Tiempo_pantalla_I])      
        dv = ua.DataValue(ua.Variant(newValue,ua.VariantType.Int16))
        entero.set_value(dv)
        client.disconnect()
        
  
text_tiempo_izq.on_change('value', update_tiempoAlternoI)
def update_alterno(attr):
    i=0
    global lista_verificacion_alterno,pre_text_consola_info
    for verificacion in lista_verificacion_alterno:
        if verificacion:
            i=i+1
    if len(lista_verificacion_alterno)== i:
        
        if b_alterno.active==1:
            print("modo alterno activado")
            pre_text_consola_info.text='MODO ALTERNO ACTIVADO'
          
             
            m241 = connect()
   
            set_=m241.get_child(['2:' + Alterno])      
       
            # Escritura del nuevo estado
            dv = ua.DataValue(ua.Variant(True,ua.VariantType.Boolean))
            set_.set_value(dv)
    
            # Desconexión del cliente
            client.disconnect()
            
        else:
            print("modo alterno desactivado")
            pre_text_consola_info.text='REALIZE OPERACION'
               
            m241 = connect()
       
            set_=m241.get_child(['2:' + Alterno])      
       
            # Escritura del nuevo estado
            dv = ua.DataValue(ua.Variant(False,ua.VariantType.Boolean))
            set_.set_value(dv)
    
            # Desconexión del cliente
            client.disconnect()
            
    else:
         pre_text_consola_info.text='DEBE INTRODUCIR LOS TIEMPOS PARA MODO ALTERNO'
b_alterno.on_click(update_alterno)
def update_TURBO(attr):
    if b_turbo.active==1:
        print("Turbo activado")
            
        m241 = connect()
   
        set_=m241.get_child(['2:' + Turbo])      
       
        # Escritura del nuevo estado
        dv = ua.DataValue(ua.Variant(True,ua.VariantType.Boolean))
        set_.set_value(dv)
    
        # Desconexión del cliente
        client.disconnect()
        
    if b_turbo.active==0:
        print("Turbo Desactivado")
 
    
            
    
        
        m241 = connect()
   
        set_=m241.get_child(['2:' + Turbo])      
       
        # Escritura del nuevo estado
        dv = ua.DataValue(ua.Variant(False,ua.VariantType.Boolean))
        set_.set_value(dv)
    
        # Desconexión del cliente
        client.disconnect()
        
b_turbo.on_click(update_TURBO)        
def update_sentido(attrname,old,new):
    if b_giro.active==0:
        print("Giro a la derecha")
        pre_text_consola_info.text='GIRO A DERECHA'       
        
        m241 = connect()
   
        set_=m241.get_child(['2:' + Sentido])      
       
        # Escritura del nuevo estado
        dv = ua.DataValue(ua.Variant(False,ua.VariantType.Boolean))
        set_.set_value(dv)
    
        # Desconexión del cliente
        client.disconnect()
            
    else:
        print("Giro Izquierda")
        pre_text_consola_info.text='GIRO A IZQUIERDA'      
        
        m241 = connect()
   
        set_=m241.get_child(['2:' + Sentido])      
       
        # Escritura del nuevo estado
        dv = ua.DataValue(ua.Variant(True,ua.VariantType.Boolean))
        set_.set_value(dv)
    
        # Desconexión del cliente
        client.disconnect()
        
b_giro.on_change('active',update_sentido)        
def update_manauto(attrname,old,new):
    global Man
 
    if b_am.active==0:
        print("Modo por pantalla")
        Man=False       
        
        m241 = connect()
   
        set_=m241.get_child(['2:' + Manual])      
       
        # Escritura del nuevo estado
        dv = ua.DataValue(ua.Variant(False,ua.VariantType.Boolean))
        set_.set_value(dv)
    
        # Desconexión del cliente
        client.disconnect()
        
              
       
    else:
        print("Modo Manual")
        Man=True       
        
        m241 = connect()
   
        set_=m241.get_child(['2:' + Manual])      
       
        # Escritura del nuevo estado
        dv = ua.DataValue(ua.Variant(True,ua.VariantType.Boolean))
        set_.set_value(dv)
    
        # Desconexión del cliente
        client.disconnect()
            
       
b_am.on_change('active', update_manauto)
def update_reset():
#  Callback para el evento asociado al botón de set
    print("pulsado botón set")
    
    m241 = connect()
   
    set_=m241.get_child(['2:' + Reset])      
    state = set_.get_value()
    # Toggle en el valor de la variable set
    new_state = True if state == False else False
    # Escritura del nuevo estado
    dv = ua.DataValue(ua.Variant(new_state,ua.VariantType.Boolean))
    set_.set_value(dv)
    
    # Desconexión del cliente
    client.disconnect()
    
# Asociación de la callback update_set al evento del botón de set
b_reset.on_click(update_reset)
#text_tiempo.on_change('value', update_set())
def update_power():
#  Callback para el evento asociado al botón de reset
    print("pulsado botón reset")
    
    m241 = connect()
    # SUSTITUIR LA DIRECCIÓN DE LA VARIABLE OPC POR LA VUESTRA (MIRAR EL NOMBRE EN EL UA EXPERT)
    reset_=m241.get_child(['2:'+VariableOn])      
    state = reset_.get_value()
    # Toggle en el valor de la variable reset
    new_state = True if state == False else False
    # Escritura del nuevo estado
    b_power.button_type= "success" if new_state else "danger"
    # Escritura del nuevo estado
    dv = ua.DataValue(ua.Variant(new_state,ua.VariantType.Boolean))
    reset_.set_value(dv)
    # Desconexión del cliente
    client.disconnect()
    
# Asociación de la callback update_reset al evento del botón de reset
b_power.on_click(update_power)
def periodicCallback():
    
#actualizar velocidad y tiempo
    
    
    
    if Man==False:
        
        m241 = connect()
        entero=m241.get_child(['2:'+ VelocidadPrograma]) 
        state = entero.get_value()     
    
    
        client.disconnect()
    #add_needle(Velocidad,'kmh')
    else:
        m241 = connect()
        entero=m241.get_child(['2:'+ Velocidad_manual]) 
        state = entero.get_value()     
    
    
        client.disconnect()
    
    new_val=[speed_to_angle(state)]
    new_val_text=[str(round(state, 2))]
    print(new_val_text)
    print(new_val)
    if new_val[0] > pi/2:
        a3.visible = False
        a2.visible = False
        a1.visible = True
        
        source_arc.data = dict(angle=new_val)
        source_text.data = dict(text=new_val_text,color=['green'])
        
        print("verde")
    elif pi/2 >= new_val[0] > 0:
        a3.visible = False
        a2.visible = True
        source_arc.data = dict(angle=[pi/2])
        source_arc_amarillo.data=dict(angle=new_val)
        source_text.data = dict(text=new_val_text,color=['yellow'])
        print("amarillo")
    elif 0>=new_val[0]>-pi/4:
        a2.visible = True
        a1.visible = True
        a3.visible = True
        source_arc.data = dict(angle=[pi/2])
        source_arc_amarillo.data=dict(angle=[0])
        source_arc_rojo.data = dict(angle=new_val)
        source_text.data = dict(text=new_val_text,color=['red'])
    
    m241 = connect()
    entero=m241.get_child(['2:'+ Tiempo_inverted]) 
    state = entero.get_value()     
    
    
    client.disconnect()
    
    new_val_t =  [speed_to_angle_t(state)]

    
    source_needle_t.data = dict(angle=new_val_t)
    
# Callback asociada a un evento periódicot
    global i  # importamos en la función la variable del contador
    
         
    m241 = connect()
    # SUSTITUIR LA DIRECCIÓN DE LA VARIABLE OPC POR LA VUESTRA (MIRAR EL NOMBRE EN EL UA EXPERT)
    if Man == False:
        entero_=m241.get_child(['2:'+VelocidadPrograma])  
    else:
        entero_=m241.get_child(['2:'+Velocidad_manual]) 
    value_y = entero_.get_value()
    # Desconectamos el cliente
    client.disconnect()
    
# =============================================================================
    # Aumentamos en 1 el contador
    i = i +1
    value_x =i
    # El nuevo par de datos formados por el nuevo valor del contador y el valor del entero
    new_data = {'x':[value_x],'y':[value_y]}
    # La función stream añade el nuevo par de datos a la variable source y
    # genera un buffer circular del tamaño indicado en el parámetro rollover 
    source.stream(new_data,rollover=100)
    
# Definición del evento periódico. En este caso el evento es lanzado cada 300 ms. No es recomendable disminuir este valor
    
doc.add_periodic_callback(periodicCallback,300)



####################################################################################
# ------------------ DISPOSICIÓN DE LOS ELEMENTOS EN EL DOCUMENTO HTML ------------#
####################################################################################


#https://github.com/bokeh/bokeh/blob/branch-2.3/examples/models/file/gauges.py


# Se incluye todos los widgets en el widgetbox


# Añadimos el widgetbox y la gráfica en una fila de ancho 800 píxeles
#grid=gridplot([div,plot_vel,imputs_control,imputs_bdd,imputs_info,plot],ncols=3,plot_width=400,plot_height=400)
#USO DE GRIDPLOT
grid=gridplot([[div,plot_vel,imputs_control,pic],[imputs_bdd,imputs_info,plot,plot_t]],plot_width=400,plot_height=400)

doc.add_root(grid)

#doc.add_root(column(plot,imputs_info))
#doc.add_root(column(imputs_control))
# Título de la página servida por bokeh
doc.title = "Simple HMI"
############################## ERRORES COMUNES ##############################
# 1. Cuidado con las indentaciones. No confundirlas con espacios.
# 2. Puede que las tildes causen problemas, en ese caso eliminarlas.
# 3. Direcciones de las variables. Comprobar que sean las correctas


###############################################
########## ABRIR CMD ###############



#os.system(r'cd C:\Users\Pelayo Leguina\Documents\integracion') 
os.system(r"bokeh serve --show simpleHMI_congauge_MEJORADO.py")






