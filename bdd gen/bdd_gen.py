# -*- coding: utf-8 -*-
"""
Created on Tue Sep 22 12:37:29 2020

@author: Pelayo Leguina
"""

#Para Bases de datos existe un modulo llamado: sqlite3

import sqlite3
import os #Modulo para gestion del sistema de archivos

#Conexion con base de datos
bdd = "BDD_MEZCLADORA.db"

#Comprobar si la base existia antes(devuelve un 1 si existe)

exists = os.path.exists(bdd)

if not exists:
    print("No existe")
else:
    print("Existe")

#Necesitamos crear un objeto "conn" (conexion)
#Crea bdd si no existe y si existe se conecta ahi.
conn=sqlite3.connect(bdd)

#Necesitamos un cursor con el que vamosa interactuar con la bdd

c = conn.cursor()


########### DDL ( CREAR TABLAS BDD)

#DEBE IR ENTRECOMILLADO, LE DICE A PYTHON QUE SON COMANDOS DE OTRO INTERPRETE (EN ESTE CASO SQL)
#codigo copiado de DBrowser para agilizar
c.execute('''
    CREATE TABLE EMPRESAS(
	"Empresa_ID"	INTEGER NOT NULL,
	"Nombre"	TEXT NOT NULL,
	"NIF"	TEXT NOT NULL,
	"Telefono"	INTEGER NOT NULL,
	PRIMARY KEY("Empresa_ID" AUTOINCREMENT)

)
    
    
    ''')
conn.commit()



c.execute('''
    CREATE TABLE PRODUCTOS(
	"Combinacion_ID"	INTEGER NOT NULL,
	"Producto_1"	TEXT NOT NULL,
    "Producto_2"     TEXT NOT NULL,
	PRIMARY KEY("Combinacion_ID" AUTOINCREMENT)
    
)
    
    
    ''')
    
conn.commit()

c.execute('''
    CREATE TABLE OPERACIONES(
	"Operacion_ID"	INTEGER NOT NULL,
	"Empresa_ID"	INTEGER NOT NULL,
	"Productos_ID"	TEXT NOT NULL,
	"Velocidad"     INTEGER NOT NULL,
    "Tiempo"    INTEGER NOT NULL,
    "Fecha"	TEXT NOT NULL,
	PRIMARY KEY("Operacion_ID" AUTOINCREMENT)
    FOREIGN KEY("Productos_ID") REFERENCES PRODUCTOS("Combinacion_ID")
    FOREIGN KEY("Empresa_ID") REFERENCES Empresas("Empresa_ID")
)
    
    
    ''')
conn.commit()

# =============================================================================
# c.execute('''
#     CREATE TABLE "PRODUCTO" (
# 	"ID_producto"	INTEGER NOT NULL,
# 	"Nombre"	TEXT NOT NULL,
# 	PRIMARY KEY("ID_producto" AUTOINCREMENT)
# );
#     
#     
#     ''')
#     
# conn.commit()
# =============================================================================

varios_productos = [('agua','arcilla'),
                 ('cal','arena'),
                 ('aceite','vinagre'),
                 ('sal','agua'),
                 ('alcohol','gel'),
                 ('pintura','aguarras'),
                 ('arcilla','agua'),
                 ('caliza','aceite'),
                 ('alquitran','arena'),
                 ('ceniza','polvo')]
#Insertar los valores de la tupla en la tabla (volcado)
c.executemany("INSERT INTO PRODUCTOS (Producto_1,Producto_2) VALUES(?,?)",varios_productos)
conn.commit()

# =============================================================================
# varios=('agua','aceite','jamon','cuarzo')
# 
# for prod in varios:
#     c.execute("INSERT INTO PRODUCTO (Nombre) VALUES (?)",prod)
# =============================================================================

conn.close()

